# mvi-sp

# Defect detection on wafer bin maps using CNN

# Assignment
Create a pipeline for defect detection on wafer bin maps using the dataset from the previous task.

# Submission
Report is here: [report.pdf](https://gitlab.fit.cvut.cz/svejsond/mvi-sp/blob/master/report.pdf)

Data need to be in same folder as the notebook for everything in notebook to work.

Source code is here: [ntb.ipynb](https://gitlab.fit.cvut.cz/svejsond/mvi-sp/blob/master/ntb.ipynb)